"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const discord_js_1 = __importDefault(require("discord.js"));
const textToSpeech = __importStar(require("@google-cloud/text-to-speech"));
const discordVoice = __importStar(require("@discordjs/voice"));
const fs_1 = __importDefault(require("fs"));
const console_stamp_1 = __importDefault(require("console-stamp"));
// import 끗
const __PREFIX = "?";
const __LOG_LEVEL = 2;
const __SAID = "  said | ";
const __VOICE = { languageCode: "ko_KR", ssmlGender: "FEMALE", name: "ko-KR-Wavenet-A" };
// 상수선언 끗
var _cs = (0, console_stamp_1.default)(console, { format: ":date(HH:MM:ss.l) :label" });
var tts = new textToSpeech.TextToSpeechClient();
var token = fs_1.default.readFileSync("token", "utf-8");
var client = new discord_js_1.default.Client({ intents: ["Guilds", "GuildMessages", "MessageContent", "GuildVoiceStates"] });
var ttsRoom = undefined;
var joined = undefined;
var player = discordVoice.createAudioPlayer({ behaviors: { noSubscriber: discordVoice.NoSubscriberBehavior.Play } });
var chatQueue = new Array;
var uptime = Date.now();
var last = "";
var timeout;
// 변수선언 끗
console.log("Hello,World!");
console.warn("Critical Error");
console.error("Ion Efflux");
// 헬로월드 발사
timeout = setTimeout(fread, 20);
function fread(e) {
    timeout = setTimeout(fread, 20);
    // 일단 타임아웃 재설정
    if (player.state.status == discordVoice.AudioPlayerStatus.Playing)
        return;
    if (joined == null)
        return;
    if (chatQueue.length == 0)
        return;
    // 쓸때없는 연산 금지
    var content = (last == chatQueue[0].name) ? chatQueue[0].chat.replace(/((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/gi, "링크") : chatQueue[0].name + __SAID + chatQueue[0].chat.replace(/((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/gi, "링크");
    last = chatQueue[0].name;
    // 변수선언 끗
    var req = {
        input: { text: content },
        voice: __VOICE,
        audioConfig: { audioEncoding: "MP3" }
    };
    try {
        tts.synthesizeSpeech(req, ftts);
    }
    catch (e) {
        console.error("TTS 생성 실패");
        timeout = setTimeout(fread, 20);
        return;
    }
    // tts 생성
    clearTimeout(timeout);
    // 잠시 타임아웃 제거
}
function ftts(e, res) {
    if (e) {
        console.error(e);
        timeout = setTimeout(fread, 20);
        // 타임아웃 재설정
        return;
    }
    // 에러같은거 있으면 리턴
    timeout = setTimeout(fread, 1100);
    // 채팅 읽는 시간을 위하여 타임아웃을 길게 잡음
    fs_1.default.writeFileSync("./tts/buffer.mp3", res.audioContent);
    player.play(discordVoice.createAudioResource("./tts/buffer.mp3"));
    chatQueue.shift();
    // tts 재생
    if (__LOG_LEVEL == 2)
        console.log("readed!");
}
// 텍스트 읽는 함수 끗
function ferror1(msg) {
    var _a;
    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
        embeds: [
            {
                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                color: 0xC00000,
                description: "TTS봇을 설치해 주세요.",
                footer: { text: msg.content },
                title: "ERROR (-1)",
                thumbnail: { url: String((_a = client.user) === null || _a === void 0 ? void 0 : _a.avatarURL()) }
            }
        ]
    }));
    if (__LOG_LEVEL == 2)
        console.warn(msg.author.tag + " : " + msg.content + " is ERROR -1");
}
//error -1 던지는 함수 끗
function fmakeTs() {
    var ts = "[";
    //[1d 12:34:56:789]
    var tmp = Date.now() - uptime;
    ts += (tmp / 1000 / 60 / 60 / 24).toFixed() + "d ";
    ts += (tmp / 1000 / 60 / 60 % 24).toFixed().padStart(2, "0") + ":";
    ts += (tmp / 1000 / 60 % 60).toFixed().padStart(2, "0") + ":";
    ts += (tmp / 1000 % 60).toFixed().padStart(2, "0") + ":";
    ts += (tmp % 1000).toFixed().padStart(3, "0") + "]";
    return ts;
}
// 업타임 함수 끗
client.login(token);
// 봇 업
client.on("ready", fready);
function fready() {
    var _a;
    if (__LOG_LEVEL == 3)
        console.log("event : ready");
    console.log(((_a = client.user) === null || _a === void 0 ? void 0 : _a.tag) + " : ON");
}
// 봇 작동시
client.on("messageCreate", fmessage);
function fmessage(msg) {
    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v;
    if (__LOG_LEVEL == 3)
        console.log("event : messageCreate");
    if (msg.author.bot)
        return;
    // 쓸때없는 연산 금지
    if (msg.content.startsWith(__PREFIX)) {
        var command = msg.content.slice(__PREFIX.length);
        var args = command.split(' ');
        // 변수선언 끗
        args[0] = args[0].toLocaleLowerCase();
        // 코드 편의를 위해서
        if (args.length == 1)
            switch (args[0]) {
                case "정보":
                case " info":
                    if (__LOG_LEVEL == 2)
                        console.log(msg.author.tag + " : " + msg.content);
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "https://gitlab.com/D0C70R_CH01/doctor_choi_the_reader  \nM4D3 I3Y 최박사 | D0C70R_CH01  \nTWITTER : @d0c70r_ch01  \nDISCORD : 최박사 | D0C70R_CH01#2885  \n업타임 : " + fmakeTs() + "  \n\n명령어는 git의 README.md 참조  \n버그문의는 git에 이슈 달거나 트위터, 디스코드 디엠  \n\nVERSION alpha2",
                                footer: { text: msg.content },
                                title: "봇 정보",
                                thumbnail: { url: String((_a = client.user) === null || _a === void 0 ? void 0 : _a.avatarURL()) }
                            }
                        ]
                    }));
                    return;
                case "핑":
                case "ping":
                    if (__LOG_LEVEL == 2)
                        console.log(msg.author.tag + " : " + msg.content);
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: client.ws.ping + "ms",
                                footer: { text: msg.content },
                                title: "서버 딜레이",
                                thumbnail: { url: String((_b = client.user) === null || _b === void 0 ? void 0 : _b.avatarURL()) }
                            }
                        ]
                    }));
                    return;
                case "설치":
                case "install":
                    if (__LOG_LEVEL == 2)
                        console.log(msg.author.tag + " : " + msg.content);
                    try {
                        ttsRoom = msg.channel;
                        chatQueue = [];
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 설치에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-2)",
                                    thumbnail: { url: String((_c = client.user) === null || _c === void 0 ? void 0 : _c.avatarURL()) }
                                }
                            ]
                        }));
                        if (__LOG_LEVEL == 2)
                            console.warn(msg.author.tag + " : " + msg.content + " is ERROR -2");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "명령어를 사용하여 음성 채널에 봇을 입장시키세요.",
                                footer: { text: msg.content },
                                title: "TTS봇 설치 완료!",
                                thumbnail: { url: String((_d = client.user) === null || _d === void 0 ? void 0 : _d.avatarURL()) }
                            }
                        ]
                    }));
                    return;
                case "제거":
                case "uninstall":
                    if (__LOG_LEVEL == 2)
                        console.log(msg.author.tag + " : " + msg.content);
                    if (ttsRoom == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    if (msg.channel != ttsRoom)
                        return;
                    //설치된 체널이 아니면 종료
                    try {
                        ttsRoom = undefined;
                        chatQueue = [];
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 제거에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-3)",
                                    thumbnail: { url: String((_e = client.user) === null || _e === void 0 ? void 0 : _e.avatarURL()) }
                                }
                            ]
                        }));
                        if (__LOG_LEVEL == 2)
                            console.warn(msg.author.tag + " : " + msg.content + " is ERROR -3");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "더이상 봇이 이 채팅방을 읽지 않습니다.",
                                footer: { text: msg.content },
                                title: "TTS봇 제거 완료!",
                                thumbnail: { url: String((_f = client.user) === null || _f === void 0 ? void 0 : _f.avatarURL()) }
                            }
                        ]
                    }));
                    return;
                case "입장":
                case "join":
                    if (__LOG_LEVEL == 2)
                        console.log(msg.author.tag + " : " + msg.content);
                    if (ttsRoom == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    if (msg.channel != ttsRoom)
                        return;
                    //설치된 체널이 아니면 종료
                    if (ttsRoom == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    try {
                        if (((_g = msg.member) === null || _g === void 0 ? void 0 : _g.voice.channelId) == undefined || ((_h = msg.member) === null || _h === void 0 ? void 0 : _h.voice.channelId) == null)
                            throw new Error("음성 채팅에 먼저 입장해 주세요.");
                        if (((_j = msg.member) === null || _j === void 0 ? void 0 : _j.voice.channelId) == ((_l = (_k = msg.guild) === null || _k === void 0 ? void 0 : _k.afkChannel) === null || _l === void 0 ? void 0 : _l.id))
                            throw new Error("잠수 채널에는 참여할 수 없습니다.");
                        joined = discordVoice.joinVoiceChannel({ channelId: (_m = msg.member) === null || _m === void 0 ? void 0 : _m.voice.channelId, guildId: (_o = msg.guild) === null || _o === void 0 ? void 0 : _o.id, adapterCreator: (_p = msg.guild) === null || _p === void 0 ? void 0 : _p.voiceAdapterCreator, selfDeaf: false, selfMute: false });
                        joined.subscribe(player);
                        chatQueue = [];
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 입장에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-4)",
                                    thumbnail: { url: String((_q = client.user) === null || _q === void 0 ? void 0 : _q.avatarURL()) }
                                }
                            ]
                        }));
                        if (__LOG_LEVEL == 2)
                            console.warn(msg.author.tag + " : " + msg.content + " is ERROR -4");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "채팅을 치면 봇이 채팅을 읽어 줍니다.",
                                footer: { text: msg.content },
                                title: "TTS봇 입장",
                                thumbnail: { url: String((_r = client.user) === null || _r === void 0 ? void 0 : _r.avatarURL()) }
                            }
                        ]
                    }));
                    return;
                case "퇴장":
                case "leave":
                    if (__LOG_LEVEL == 2)
                        console.log(msg.author.tag + " : " + msg.content);
                    if (ttsRoom == undefined) {
                        ferror1(msg);
                        return;
                    }
                    //설치가 안되어있으면 종료
                    if (msg.channel != ttsRoom)
                        return;
                    //설치된 체널이 아니면 종료
                    try {
                        if (joined == undefined)
                            return;
                        joined.destroy();
                        joined = undefined;
                    }
                    catch (e) {
                        msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                            embeds: [
                                {
                                    author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                    color: 0xC00000,
                                    description: "TTS봇 퇴장에 실패하였습니다.\n" + e.toString(),
                                    footer: { text: msg.content },
                                    title: "ERROR (-5)",
                                    thumbnail: { url: String((_s = client.user) === null || _s === void 0 ? void 0 : _s.avatarURL()) }
                                }
                            ]
                        }));
                        if (__LOG_LEVEL == 2)
                            console.warn(msg.author.tag + " : " + msg.content + " is ERROR -5");
                        return;
                    }
                    msg.channel.send(new discord_js_1.default.MessagePayload(msg.channel, {
                        embeds: [
                            {
                                author: { name: msg.author.username, icon_url: String(msg.author.avatarURL()) },
                                color: 0x0080C0,
                                description: "더이상 봇이 채팅을 읽어주지 않습니다.",
                                footer: { text: msg.content },
                                title: "TTS봇 퇴장",
                                thumbnail: { url: String((_t = client.user) === null || _t === void 0 ? void 0 : _t.avatarURL()) }
                            }
                        ]
                    }));
                    chatQueue = [];
                    return;
            }
        //명령어 처리
    }
    // 명령어 접두로 시작할 시
    if (msg.channel == ttsRoom) {
        if (ttsRoom == undefined || joined == undefined)
            return;
        if (((_u = msg.member) === null || _u === void 0 ? void 0 : _u.voice.channelId) != undefined || ((_v = msg.member) === null || _v === void 0 ? void 0 : _v.voice.channelId) != null && ttsRoom != undefined)
            chatQueue.push({ name: msg.member.displayName, chat: msg.content });
        // 아닌 경우
    }
    if (__LOG_LEVEL == 2)
        console.log(msg.author.tag + " " + msg.content + " is added");
    // console.log(chatQueue);
}
//메세지 처리 함수 끗
