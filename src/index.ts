import discord from "discord.js";
import * as textToSpeech from "@google-cloud/text-to-speech";
import * as discordVoice from "@discordjs/voice";
import fs from "fs";
import consoleStamp from "console-stamp";
// import 끗

const __PREFIX: string = "?";
const __LOG_LEVEL: number = 2;
const __SAID: string = "  said | ";
const __VOICE: textToSpeech.protos.google.cloud.texttospeech.v1.IVoiceSelectionParams = {languageCode: "ko_KR", ssmlGender: "FEMALE", name: "ko-KR-Wavenet-A"};
// 상수선언 끗

var _cs = consoleStamp(console, {format: ":date(HH:MM:ss.l) :label"});
var tts: textToSpeech.TextToSpeechClient = new textToSpeech.TextToSpeechClient();
var token = fs.readFileSync("token", "utf-8");
var client: discord.Client = new discord.Client({intents: ["Guilds", "GuildMessages", "MessageContent", "GuildVoiceStates"]});
var ttsRoom: discord.DMChannel | discord.PartialDMChannel | discord.NewsChannel | discord.TextChannel | discord.PublicThreadChannel | discord.PrivateThreadChannel | discord.VoiceChannel | undefined = undefined;
var joined: discordVoice.VoiceConnection | undefined = undefined;
var player: discordVoice.AudioPlayer = discordVoice.createAudioPlayer({behaviors: {noSubscriber: discordVoice.NoSubscriberBehavior.Play}});
var chatQueue: Array<{name: string, chat: string}> = new Array<{name: string, chat: string}>;
var uptime: number = Date.now();
var last: string = "";
var timeout: number;
// 변수선언 끗

console.log("Hello,World!");
console.warn("Critical Error");
console.error("Ion Efflux");
// 헬로월드 발사

timeout = setTimeout(fread, 20);
function fread(e: Array<{name: string, chat: string}>): void
{
    timeout = setTimeout(fread, 20);
    // 일단 타임아웃 재설정

    if(player.state.status == discordVoice.AudioPlayerStatus.Playing) return;
    if(joined == null) return;
    if(chatQueue.length == 0) return;
    // 쓸때없는 연산 금지
    
    var content: string = (last == chatQueue[0].name)? chatQueue[0].chat.replace(/((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/gi, "링크") : chatQueue[0].name + __SAID + chatQueue[0].chat.replace(/((http[s]?|ftp):\/)?\/?([^:\/\s]+)((\/\w+)*\/)([\w\-\.]+[^#?\s]+)(.*)?(#[\w\-]+)?$/gi, "링크");
    last = chatQueue[0].name;
    // 변수선언 끗

    var req: textToSpeech.protos.google.cloud.texttospeech.v1.ISynthesizeSpeechRequest | undefined = 
    {
        input: {text: content},
        voice: __VOICE,
        audioConfig: {audioEncoding: "MP3"}
    };
    try
    {
        tts.synthesizeSpeech(req, ftts);
    }
    catch(e: any)
    {
        console.error("TTS 생성 실패");
        timeout = setTimeout(fread, 20);
        return;
    }
    // tts 생성

    clearTimeout(timeout);
    // 잠시 타임아웃 제거
}
function ftts(e: any, res: any): void
{
    if(e)
    {
        console.error(e);
        timeout = setTimeout(fread, 20);
        // 타임아웃 재설정

        return;
    }
    // 에러같은거 있으면 리턴

    timeout = setTimeout(fread, 1100);
    // 채팅 읽는 시간을 위하여 타임아웃을 길게 잡음

    fs.writeFileSync("./tts/buffer.mp3", res.audioContent);
    player.play(discordVoice.createAudioResource("./tts/buffer.mp3"));
    chatQueue.shift();
    // tts 재생

    if(__LOG_LEVEL == 2)  console.log("readed!");
}
// 텍스트 읽는 함수 끗

function ferror1(msg: discord.Message)
{
    msg.channel.send(new discord.MessagePayload(msg.channel, 
        {
            embeds: [
            {
                author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                color: 0xC00000, 
                description: "TTS봇을 설치해 주세요.", 
                footer: {text: msg.content}, 
                title: "ERROR (-1)", 
                thumbnail: {url: String(client.user?.avatarURL())}
            }]
        }));

        if(__LOG_LEVEL == 2) console.warn(msg.author.tag + " : " + msg.content + " is ERROR -1")
}
//error -1 던지는 함수 끗

function fmakeTs(): string
{
    var ts: string = "[";
    //[1d 12:34:56:789]

    var tmp : number = Date.now() - uptime;
    ts += (tmp / 1000 / 60 / 60 /24).toFixed() + "d ";
    ts += (tmp / 1000 / 60 / 60 % 24).toFixed().padStart(2, "0") + ":";
    ts += (tmp / 1000 / 60 % 60).toFixed().padStart(2, "0") + ":";
    ts += (tmp / 1000 % 60).toFixed().padStart(2, "0") + ":";
    ts += (tmp % 1000).toFixed().padStart(3, "0") + "]";

    return ts;
}
// 업타임 함수 끗

client.login(token);
// 봇 업

client.on("ready", fready);
function fready(): void
{
    if(__LOG_LEVEL == 3) console.log("event : ready");

    console.log(client.user?.tag + " : ON");
}
// 봇 작동시

client.on("messageCreate", fmessage);
function fmessage(msg: discord.Message): void
{
    if(__LOG_LEVEL == 3) console.log("event : messageCreate");

    if(msg.author.bot) return;
    // 쓸때없는 연산 금지

    if(msg.content.startsWith(__PREFIX))
    {
        var command: string = msg.content.slice(__PREFIX.length);
        var args: Array<string> = command.split(' ');
        // 변수선언 끗

        args[0] = args[0].toLocaleLowerCase();
        // 코드 편의를 위해서

        if(args.length == 1) switch(args[0])
        {
            case "정보": 
            case " info": 
                if(__LOG_LEVEL == 2) console.log(msg.author.tag + " : " + msg.content);

                msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0x0080C0, 
                            description: "https://gitlab.com/D0C70R_CH01/doctor_choi_the_reader  \nM4D3 I3Y 최박사 | D0C70R_CH01  \nTWITTER : @d0c70r_ch01  \nDISCORD : 최박사 | D0C70R_CH01#2885  \n업타임 : " + fmakeTs() + "  \n\n명령어는 git의 README.md 참조  \n버그문의는 git에 이슈 달거나 트위터, 디스코드 디엠  \n\nVERSION alpha2", 
                            footer: {text: msg.content}, 
                            title: "봇 정보", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    }));
                return;
            case "핑":
            case "ping":
                if(__LOG_LEVEL == 2) console.log(msg.author.tag + " : " + msg.content);

                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: client.ws.ping + "ms", 
                        footer: {text: msg.content}, 
                        title: "서버 딜레이", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                }));
                return;
            case "설치":
            case "install":
                if(__LOG_LEVEL == 2) console.log(msg.author.tag + " : " + msg.content);

                try
                {
                    ttsRoom = msg.channel;
                    chatQueue = [];
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 설치에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-2)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    }));

                    if(__LOG_LEVEL == 2) console.warn(msg.author.tag + " : " + msg.content + " is ERROR -2")
                    return;
                }
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0x0080C0, 
                            description: "명령어를 사용하여 음성 채널에 봇을 입장시키세요.", 
                            footer: {text: msg.content}, 
                            title: "TTS봇 설치 완료!", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    }));
                return;
            case "제거":
            case "uninstall":
                if(__LOG_LEVEL == 2) console.log(msg.author.tag + " : " + msg.content);

                if(ttsRoom == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                if(msg.channel != ttsRoom) return;
                //설치된 체널이 아니면 종료

                try
                {
                    ttsRoom = undefined;
                    chatQueue = [];
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 제거에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-3)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    }));

                    if(__LOG_LEVEL == 2) console.warn(msg.author.tag + " : " + msg.content + " is ERROR -3")
                    return;
                }
                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: "더이상 봇이 이 채팅방을 읽지 않습니다.", 
                        footer: {text: msg.content}, 
                        title: "TTS봇 제거 완료!", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                }));
                return;
            case "입장":
            case "join":
                if(__LOG_LEVEL == 2) console.log(msg.author.tag + " : " + msg.content);

                if(ttsRoom == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                if(msg.channel != ttsRoom) return;
                //설치된 체널이 아니면 종료

                if(ttsRoom == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                try
                {
                    if(msg.member?.voice.channelId == undefined || msg.member?.voice.channelId == null) throw new Error("음성 채팅에 먼저 입장해 주세요.");
                    if(msg.member?.voice.channelId == msg.guild?.afkChannel?.id) throw new Error("잠수 채널에는 참여할 수 없습니다.");

                    joined = discordVoice.joinVoiceChannel({channelId: msg.member?.voice.channelId as string, guildId: msg.guild?.id as string, adapterCreator: msg.guild?.voiceAdapterCreator as discordVoice.DiscordGatewayAdapterCreator, selfDeaf: false, selfMute: false});
                    joined.subscribe(player);

                    chatQueue = [];
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 입장에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-4)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    }));

                    if(__LOG_LEVEL == 2) console.warn(msg.author.tag + " : " + msg.content + " is ERROR -4")
                    return;
                }
                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: "채팅을 치면 봇이 채팅을 읽어 줍니다.", 
                        footer: {text: msg.content}, 
                        title: "TTS봇 입장", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                }));
                return;
            case "퇴장":
            case "leave":
                if(__LOG_LEVEL == 2) console.log(msg.author.tag + " : " + msg.content);

                if(ttsRoom == undefined)
                {
                    ferror1(msg);
                    return;
                }
                //설치가 안되어있으면 종료

                if(msg.channel != ttsRoom) return;
                //설치된 체널이 아니면 종료

                try
                {
                    if(joined == undefined) return;

                    joined.destroy();
                    joined = undefined;
                }
                catch(e: any)
                {
                    msg.channel.send(new discord.MessagePayload(msg.channel, 
                    {
                        embeds: [
                        {
                            author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                            color: 0xC00000, 
                            description: "TTS봇 퇴장에 실패하였습니다.\n" + e.toString(), 
                            footer: {text: msg.content}, 
                            title: "ERROR (-5)", 
                            thumbnail: {url: String(client.user?.avatarURL())}
                        }]
                    }));

                    if(__LOG_LEVEL == 2) console.warn(msg.author.tag + " : " + msg.content + " is ERROR -5")
                    return;
                }
                msg.channel.send(new discord.MessagePayload(msg.channel, 
                {
                    embeds: [
                    {
                        author: {name: msg.author.username, icon_url: String(msg.author.avatarURL())}, 
                        color: 0x0080C0, 
                        description: "더이상 봇이 채팅을 읽어주지 않습니다.", 
                        footer: {text: msg.content}, 
                        title: "TTS봇 퇴장", 
                        thumbnail: {url: String(client.user?.avatarURL())}
                    }]
                }));
                chatQueue = [];
                return;
        }
        //명령어 처리
    }
    // 명령어 접두로 시작할 시

    if(msg.channel == ttsRoom)
    {
        if(ttsRoom == undefined || joined == undefined) return;
        if(msg.member?.voice.channelId != undefined || msg.member?.voice.channelId != null && ttsRoom != undefined) chatQueue.push({name: msg.member.displayName, chat: msg.content});
        // 아닌 경우
    }

    if(__LOG_LEVEL == 2) console.log(msg.author.tag + " " + msg.content + " is added");

    // console.log(chatQueue);
}
//메세지 처리 함수 끗